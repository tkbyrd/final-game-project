﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PetSitter/Item")]
public class Item : ScriptableObject {

    public string itemName;
    
}
