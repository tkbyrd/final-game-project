﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    List<string> actionLog = new List<string>();
    public Text displayText;
    


    // Use this for initialization
    void Awake () {
		
        

	}

    public void DisplayLoggedText()
    {
        string logAsText = string.Join("\n", actionLog.ToArray());

        displayText.text = logAsText;
    }
	
	// Update is called once per frame
	void Update () {
	
        

	}

    void Start()
    {
        DisplayLoggedText();
    }

    public void LogStringWithReturn(string stringToAdd)
    {
        actionLog.Add(stringToAdd + "\n");
    }
}
