﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInput : MonoBehaviour {

    // Use this for initialization
    //void Start () {

    //}

    // Update is called once per frame
    //void Update () {

    //}
    public InputField inputField;
    private GameManager gameManager;
    private InventoryManager inventoryManager;

    void Awake()
    {
        inventoryManager = GetComponent<InventoryManager>();
        gameManager = GetComponent<GameManager>();
        inputField.onEndEdit.AddListener(AcceptStringInput);
    }

    //Send commands to be executed.

    //State: FUNCTIONAL
    void AcceptStringInput(string userInput)
    {
        userInput = userInput.ToLower();
        //manager.LogStringWithReturn(userInput);
        inventoryManager.AttemptCommand(userInput);

    }

    void InputComplete()
    {
        //manager.DisplayLoggedText();
        //inputField.ActivateInputField();
        //This draws the focus back on the InputField object.
        inputField.text = null;
    }
    
}
