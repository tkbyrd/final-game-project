﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [HideInInspector] public InventoryManager inventoryManager;


    public Button FoodGetButton;
    public Button WaterGetButton;
    public Button BallGetButton;
    public Button BoneGetButton;

    public Text DisplayText;
    public Text WinText;

    public Button FoodButton;
    public Button WaterButton;
    public Button BallButton;
    public Button BoneButton;
    public Button LetterButton;
    public SpriteRenderer FullLetterOverlay;
    private bool isViewingLetter = false;

    public SpriteRenderer StandingSchnauzer;
    public SpriteRenderer SittingSchnauzer;
    public SpriteRenderer LayingSchnauzer;
    public SpriteRenderer PlayingSchnauzer;
    private bool hasFood = false;
    private bool hasWater = false;
    public bool hasDoneTrick = false;
    public bool hasTreat = false;
    public bool hasBall = false;
    public bool hasBone = false;

    public SpriteRenderer FoodBowl;
    public SpriteRenderer WaterBowl;
    public SpriteRenderer Ball;
    public SpriteRenderer BoneTreat;

    public SpriteRenderer BallInMouth;
    public SpriteRenderer BoneTreatInMouth;

    public SpriteRenderer HeartIconRight;
    public SpriteRenderer HeartIconMiddle;
    public SpriteRenderer HeartIconLeft;
    private bool hasPerformedTrick = false;

    // Use this for initialization
    void Awake()
    {
        inventoryManager = GetComponent<InventoryManager>();

        //Set all menu buttons except for the letter to invisible / unplayable.
        FoodButton.gameObject.SetActive(false);
        WaterButton.gameObject.SetActive(false);
        BallButton.gameObject.SetActive(false);
        BoneButton.gameObject.SetActive(false);

        //Set passive feedback items to invisible.
        FullLetterOverlay.gameObject.SetActive(false);
        DisableAllItems();

        //Set other poses to invisible.
        DisableAllPoses();
        StandingSchnauzer.gameObject.SetActive(true);

        //Set goal icons to invisible.
        HeartIconLeft.gameObject.SetActive(false);
        HeartIconMiddle.gameObject.SetActive(false);
        HeartIconRight.gameObject.SetActive(false);

        //Set win text to invisible.
        WinText.gameObject.SetActive(false);

    }

	// Update is called once per frame
	void Update () {
		
	}

    //When a get item button is clicked, add the item to the inventory
    //and disable the get item button.

    //State: FUNCTIONAL
    public void ItemIsTaken(GetButton activeButton)
    {
        if (isViewingLetter != true)
        {
            string itemTaken = activeButton.itemToGet;
            activeButton.gameObject.SetActive(false);
            inventoryManager.GetComponent<InventoryManager>();
            inventoryManager.AddItem(itemTaken);
            CheckButtonToAdd();
            DisplayText.text = "You took the " + itemTaken + ".";
        }
        
    }

    //If the player has clicked the button to read the letter,
    //set the overlay to visible, and disable button input.
    //If the player is already viewing the letter,
    //set the overlay to invisible, and reactive button input.

    //State: FUNCTIONAL
    public void IsReadingLetter()
    {
        if(isViewingLetter)
        {
            FullLetterOverlay.gameObject.SetActive(false);
            isViewingLetter = false;
        }
        else
        {
            FullLetterOverlay.gameObject.SetActive(true);
            isViewingLetter = true;
        }
        
    }

    //If the respective item has been attained,
    //set the corresponding menu button to active.

    //State: FUNCTIONAL
    public void CheckButtonToAdd()
    {
        if(inventoryManager.currentItems.Contains("food"))
        {
            FoodButton.gameObject.SetActive(true);
        }
        if(inventoryManager.currentItems.Contains("water"))
        {
            WaterButton.gameObject.SetActive(true);
        }
        if (inventoryManager.currentItems.Contains("ball"))
        {
            BallButton.gameObject.SetActive(true);
        }
        if (inventoryManager.currentItems.Contains("bone"))
        {
            BoneButton.gameObject.SetActive(true);
        }
    }

    public void HitFoodButton()
    {
        if (isViewingLetter != true)
        {
            DisplayText.text = "You fed Sasha.";
            DisableAllItems();
            DisableAllPoses();
            FoodBowl.gameObject.SetActive(true);
            LayingSchnauzer.gameObject.SetActive(true);
            hasFood = true;
            hasDoneTrick = false;
            hasBone = false;
            CheckHeartsToAdd();
        }
    }

    public void HitBallButton()
    {
        if (isViewingLetter != true)
        {
            DisplayText.text = "You gave Sasha her ball. She loves it!";
            DisableAllItems();
            DisableAllPoses();
            Ball.gameObject.SetActive(true);
            PlayingSchnauzer.gameObject.SetActive(true);
            hasBall = true;
            hasDoneTrick = false;
            hasBone = false;
            CheckHeartsToAdd();
        }
    }

    public void HitBoneButton()
    {
        if (isViewingLetter != true)
        {
            DisplayText.text = "You gave Sasha a treat.";
            DisableAllItems();
            DisableAllPoses();
            BoneTreat.gameObject.SetActive(true);
            SittingSchnauzer.gameObject.SetActive(true);
            hasBone = true;
            CheckHeartsToAdd();
        }
    }

    public void HitWaterButton()
    {
        if (isViewingLetter != true)
        {
            DisplayText.text = "You gave Sasha the water.";
            DisableAllItems();
            DisableAllPoses();
            WaterBowl.gameObject.SetActive(true);
            LayingSchnauzer.gameObject.SetActive(true);
            hasWater = true;
            hasDoneTrick = false;
            hasBone = false;
            CheckHeartsToAdd();
        }
    }

    //If the player has fulfilled one of the requirements,
    //set the respective heart icon to visible.

    //State: UNTESTED
    public void CheckHeartsToAdd()
    {
        if(hasFood && hasWater)
        {
            HeartIconLeft.gameObject.SetActive(true);
        }
        if(hasBall)
        {
            HeartIconMiddle.gameObject.SetActive(true);
        }
        if(hasDoneTrick && hasBone)
        {
            HeartIconRight.gameObject.SetActive(true);
        }

        if(hasFood && hasWater && hasBall && hasDoneTrick && hasBone)
        {
            WinText.gameObject.SetActive(true);
        }
    }

    public void DisableAllPoses()
    {
        //Set all poses to invisible.
        StandingSchnauzer.gameObject.SetActive(false);
        SittingSchnauzer.gameObject.SetActive(false);
        LayingSchnauzer.gameObject.SetActive(false);
        PlayingSchnauzer.gameObject.SetActive(false);
    }

    public void DisableAllItems()
    {
        //Set all items to invisible.
        FoodBowl.gameObject.SetActive(false);
        WaterBowl.gameObject.SetActive(false);
        Ball.gameObject.SetActive(false);
        BoneTreat.gameObject.SetActive(false);
        BallInMouth.gameObject.SetActive(false);
        BoneTreatInMouth.gameObject.SetActive(false);
    }
}
