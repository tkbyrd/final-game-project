﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour {

    const int CommandsUntilWontListen = 3;
    private GameManager manager;
    public List<string> currentItems;
    public int commandsAfterName = 0;
    private string[] tricks = new string[] { "sasha", "sit", "lay", "fetch", "ball", "bone" }; //bark, speak, play

    //Takes an item's name and adds it to the current inventory.

    //State: FUNCTIONAL
    public void AddItem(string itemName)
    {
        currentItems.Add(itemName);
        DisplayInventoryText();
    }

    //Strictly for testing the contents of the list,
    //Displays all contents of currentItems in the console.

    //State: FUNCTIONAL
    public void DisplayInventoryText()
    {
        string combinedText = "";
        for (int x = 0; x < currentItems.Count; x++)
        {
            combinedText = combinedText + currentItems[x] + "\n";
        }
        //print(combinedText);

    }

    void Awake()
    {
        manager = GetComponent<GameManager>();
    }

    /////////////////////////////////////////////

    //State: Incomplete
    public void AttemptCommand(string command)
    {
        bool correctCommand = false;
        for (int x = 0; x < tricks.Length; x++)
        {
            if (command.Equals(tricks[x]))
            {
                correctCommand = true;
            }
        }
        if (correctCommand)
        {
            if (command.Equals("sasha"))
            {
                commandsAfterName = 0;
                manager.hasDoneTrick = false;
                manager.DisplayText.text = "It looks like she heard you call!";
            }
            else if (commandsAfterName >= CommandsUntilWontListen)
            {
                commandsAfterName += 1;
                manager.hasDoneTrick = false;
                manager.DisplayText.text = "Sasha wasn't listening to you.";
                manager.DisableAllItems();
                manager.DisableAllPoses();
                manager.StandingSchnauzer.gameObject.SetActive(true);
            }
            else
            {
                manager.DisplayText.text = "You told Sasha, \"" + command + ".\"";
                manager.hasDoneTrick = true;
                if (command.Equals("sit"))
                {
                    commandsAfterName += 1;
                    manager.DisableAllItems();
                    manager.DisableAllPoses();
                    manager.SittingSchnauzer.gameObject.SetActive(true);
                }
                else if (command.Equals("lay"))
                {
                    commandsAfterName += 1;
                    manager.DisableAllItems();
                    manager.DisableAllPoses();
                    manager.LayingSchnauzer.gameObject.SetActive(true);
                }
                else if (command.Equals("fetch") || command.Equals("ball"))
                {
                    if (manager.hasBall)
                    {
                        commandsAfterName += 1;
                        manager.DisableAllItems();
                        manager.DisableAllPoses();
                        manager.PlayingSchnauzer.gameObject.SetActive(true);
                        manager.BallInMouth.gameObject.SetActive(true);
                    }
                    else
                    {
                        commandsAfterName += 1;
                        manager.hasDoneTrick = false;
                        manager.DisplayText.text = "Sasha couldn't find her ball.";
                    }
                    
                }
                else if (command.Equals("bone"))
                {
                    if (manager.hasBone)
                    {
                        commandsAfterName += 1;
                        manager.DisableAllItems();
                        manager.DisableAllPoses();
                        manager.SittingSchnauzer.gameObject.SetActive(true);
                        manager.BoneTreatInMouth.gameObject.SetActive(true);
                    }
                    else
                    {
                        commandsAfterName += 1;
                        manager.hasDoneTrick = false;
                        manager.DisplayText.text = "Sasha couldn't find her bone.";
                    }

                }
            }
            
        }
        else
        {
            manager.DisplayText.text = "Sasha doesn't understand the command " + command + ".";
            manager.hasDoneTrick = false;
        }

        print(commandsAfterName);

    }



}
